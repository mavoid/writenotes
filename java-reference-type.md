树遍历解释java引用类型
======

<div>编程</div>

<time>2017/3/22</time>

Java中所有的变量都可以看做是一个引用，和C中的指针类似。虽然是共识，但是在实际开发中稍一不慎也会遇到致命的bug。

#### 一个经典的算法

已知二叉树的前序遍历序列和中序遍历序列我们可以唯一确定它的后序遍历序列。基本的想法是用前序遍历序列确定根节点，用确定了的根节点在中序遍历序列中确定左右子树，然后递归求解左右子树的后序遍历序列，最后合并求解结果。

#### java引用类型潜在的危险

如果你选择用java实现这个递归过程，在不清楚java的引用类型机制的情况下，很容易写出错误的代码。关于java引用类型，请阅读 [Reference Types](https://www.safaribooksonline.com/library/view/java-8-pocket/9781491901083/ch04.html)。

引用类型，在树遍历这个问题中，表示的是在整个递归过程中，所有用函数的参数赋值的变量，都共享同一个值，修改其中任意一个变量的值，其它共享这一对象的变量的值也会改变（俗称副作用）。对值类型和引用类型更加详细的讨论可以查看我的 [另一篇文章]()。

下面是完整的代码，（可以编译通过并运行）

```java
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class PostOrder {
    public static List getPostOrder(List<Integer> preOrder, List<Integer> middleOrder){
        if (preOrder.size() == 1){
            return new ArrayList<>(preOrder);  // return a copy
//      //如果使用下面的代码而不是 new（分配新的内存） 一个ArrayList
//      //修改leftPostOrder的值（下边代码可以看到）会连带修改最初传进来的参数preOrder的值
//          return preOrder;  // return reference
        }else if (preOrder.size() == 0){
            return new ArrayList<>();
        }
        //找到根节点
        Integer root  = preOrder.get(0);
        int index = middleOrder.indexOf(root);
        //在前序遍历序列中提取出左右子树
        List leftPre = preOrder.subList(1, index + 1);
        List rightPre = preOrder.subList(index+1, preOrder.size());
        //在中序遍历序列中提取出左右子树
        List leftMiddle = middleOrder.subList(0, index);
        List rightMiddle = middleOrder.subList(index + 1, middleOrder.size());
        //递归求解左子树
        List leftPostOrder = getPostOrder(leftPre, leftMiddle);
        //递归求解右子树
        List rightPostOrder = getPostOrder(rightPre, rightMiddle);
        // 因为java的ArrayList是引用类型
        // 以上的变量leftPre，rightPre，leftMiddle，rightMiddle均可看做是指针
        //合并求解结果
        leftPostOrder.addAll(rightPostOrder);  //修改leftPostOrder的值
        leftPostOrder.add(root);
        return leftPostOrder;
        //被注释的代码是另一种解决方案，解决的方法同样是分配新的内存（new ArrayList）
//        List result = new ArrayList<>();
//        result.addAll(leftPostOrder);
//        result.addAll(rightPostOrder);
//        result.add(root);
//        return result;
    }
    public static void main(String[] args){
        List pre = new ArrayList();
        pre.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7));
        List middle = new ArrayList();
        middle.addAll(Arrays.asList(3, 2, 4, 1, 6, 5, 7));
        System.out.println(getPostOrder(pre, middle));
        // => [3, 4, 2, 6, 7, 5, 1]
    }
}
```

#### 怎么找到bug

在参数传递的过程中，java的引用类型（继承自）传递的是引用（可以理解为C语言中的指针，或者C++中的引用），允许在函数执行体内修改变量的值。在本文给出的的代码中，leftPre是来自于preOrder[1, 2, 3, 4, 5, 6, 7]的引用，当

//递归求解左子树
```java
List leftPostOrder = getPostOrder(leftPre, leftMiddle);
```
代码执行，并且递归到leftPre的大小为1时，
```java
if (preOrder.size() == 1){
    return preOrder; // return reference
}
```
此时leftPostOrder也成为了preOrder[1, 2, 3, 4, 5, 6, 7]的引用，下边的代码就会执行，
```java
leftPostOrder.addAll(rightPostOrder); //修改leftPostOrder的值
```
修改```leftPostOrder```的值，同时preOrder[1, 2, 3, 4, 5, 6, 7]被修改，程序中其他部分在使用```preOrder```及其衍生出的所有引用时，将产生不可预测的结果。

另一种解决方法被注释掉了，和刚才的方法类似，都是分配新的内存用于存储计算结果，只是分配内存的时机稍有差别，具体请看完整代码。

java引用类型导致的安全问题
java用```private```关键字实现面向对象编程里的数据封装，将类的隐私数据隐藏为类外不可见，但是下边的代码风格将导致安全访问漏洞。

```java
public class PrivateAccess {
	private String[] rootUsers = {"root1", "root2"};
    public String[] getRootUsers(){
    	return rootUsers;
    }
    public void access(String user){
    	if ( /* user in rootUser*/){
        	// access critical data
        }else{
        	throw new IllegalUserException();
        }
    }
}
```

rootUsers（String[]）的引用类型特点导致rootUsers暴露给类的调用者。

```java
PrivateAccess access = new PrivateAccess();
access.getRootUsers()[0] = "illegalUser1";
access.access("illegalUser1"); //access critical data using non-rootUsers
```
而避免问题出现的方法就是返回数据的copy而不是直接返回引用。

```java
public String[] getRootUsers(){
    	return rootUsers.clone();  // return a copy
}
```