使用静态ip模式共享电脑网络给路由器
=========

注意事项
------
设置好路由器与以太网在同一个子网之后，一定要重新选择一下被共享网卡的共享选项。
