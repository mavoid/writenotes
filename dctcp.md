DCTCP Paper
========

扩展阅读
------
TCP协议的两大家族
- 基于延迟
- 主动队列管理（QAM）

ECN
- [ECN form wikipedia](https://en.wikipedia.org/wiki/Explicit_Congestion_Notification)


RED
- As the queue grows, the probability for dropping an incoming packet grows too. When the buffer is full, the probability has reached 1 and all incoming packets are dropped.（from wikipedia）

统计复用
- 按需分配的思想，所有实体共用同一块资源，以达到最大**效用（utility）**
- 要实现公平（fairness）分配

TCP
- 收敛时间。一个新流到达开始，到这条流获得公平的带宽分配所用的时间。

术语对照表
--------
- [x] state of art. sometimes "cutting edge".最先进的
- [x] back(fore)-ground traffic.
- [x] key metric.关键指标
- [ ] SLA