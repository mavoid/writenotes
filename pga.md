##PGA:Using Graphs to Express and Automatically Reconcile Network Policies. Paper review by Bryce Ma

policy中文翻译成**规则**或者**策略**

PGA支持ACL规则，和service chain（比如包含防火墙、负载均衡等）规则

	A service chain simply consists of a set of network services, such as firewalls or application delivery controllers (ADCs) that are interconnected through the network to support an application. 

网络中的规则主要和以下的功能有关：连通性、安全性、性能、权限设置
规则可以是静态的，或者动态的（比如在某些条件下触发）

在企业级的网络中，在同一个网络中存在不同的规则设置者

==初步的设想==：将这些独立的规则（由不同的规则设置者制定的）部署到物理设备上之前，自动检测和消除规则之间的冲突，将所有规则组合成一个没有冲突的规则集合。

使策略需求描述与底层物理架构分离

==主要贡献==：
- 提出**规则的图抽象模型（i.e. PGA）**.PGA很自然地包含middlebox
- 


策略的自动组合（composition）由系统来完成，而不是易出错的人类

网络管理员和云中心租户在白板上画图表来定义网络策略

 Currently, the Internet Engineering Task Force (IETF) is working on a standard policy framework and related protocols.

寻找不同：

定义（什么是）：
label


