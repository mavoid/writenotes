
对佛教的误解
-------
<div>文化</div>

<time>2017/4/5</time>


许多人常常是不深入了解一件事，而一直根据某些刻板印象来解释它们，对于佛教也是一样，然而不深入了解就去谈论它们是有失公平的。我会根据我读到的关于佛教的书来说出我的观点。鉴于我现阶段对佛教的肤浅认识，话不可全信 :)。因为佛教发展千年，分支众多，教义各不相同，我只谈论其哲学的部分。参考书主要是《佛陀的启示》和《正见》，都是佛教的科普读物，并没有高深的理论。

##### 佛是佛教里的神，和耶稣在基督教里神的地位一样
佛由梵文音译而来，原意为“智慧”、“觉悟”。佛教里的日常修行，就是为了成佛。所以佛并不是神的化身，佛是人经过刻苦的修行而大彻大悟的人。所以说人人可以成佛。那些整天只烧香拜佛的人而不注重个人日常生活中修行的人其实是偏离了佛教的教义的。佛教的中心在于众生，因为个人（我们自己）也是众生中的一员，所以佛教是很注重个人觉悟的，觉悟了的人如果怀有慈悲之心，也会主动帮助其他希望觉悟的人走向觉悟，即“普度众生”，这些人才有资格被人称为“大师”。

和之前我接触的宗教不同的是，佛教是无神论的，而且是和个人生活联系最紧密的“宗教”。

##### 佛教是那些信教的人的事情，离我很远
对佛教不了解的人往往认为信仰佛教就要进行佛事活动，或者穿着僧衣，遵循戒律等等。我个人的观点有可能太偏颇，我认为佛教的本质是它的理念而不是外在表现，在我看来身穿僧衣的人并不会比不穿的更加接近“觉悟”与“智慧”，换句话说就是佛在心中，而不是仅仅通过掌握佛法和背诵佛经而得到的。在这方面走的更远的就是克里希那穆提，被佛教教众认为是“彻悟的觉者”，然而宣称脱离任何宗教，解散自己的追随者们，“一旦追随某个人，你就不再追随真理”。佛教更加接近哲学而不是宗教。

佛教关注当下，因为佛教认为过去和未来的自己都不是真实的自己。通过“临在”，避免自己深陷在过去和未来里，脱离苦难。所以佛教是关注个人的从苦难中解脱的。这方面不介绍太多，感兴趣可以找书看看。

##### 佛教中匪夷所思的神话故事
因为关于释迦摩尼各种民间的神话故事在流传，所以有些研究宗教的人就拿此攻击佛教的神话倾向，其实是舍本逐末的做法。因为缺少文字记载以及年代久远，释迦摩尼的故事在世代口口相传中不免加入神话的元素也无可厚非。用此来一味否定佛教里的哲学理念是逻辑上不成立的，就像有人说的，人和ta说的话是不完全等同的，圣人说蠢话也是常有的事。

想消除误解除了潜心研究它，并无其它的捷径，佛教算是我遇到的这么多哲学理念里比较是符合我口味的，希望自己有深入了解的机会。