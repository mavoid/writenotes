<TeXmacs|1.99.4>

<style|<tuple|generic|english>>

<\body>
  <very-large|<with|font-series|bold|<with|font-shape|small-caps|Jan
  Kowalski>>><next-line><hrule>

  <\with|par-mode|right>
    <small|<tabular|<tformat|<cwith|4|4|1|1|cell-halign|r>|<cwith|3|3|1|1|cell-halign|r>|<cwith|2|2|1|1|cell-halign|r>|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|Main
    Street 123/4>>|<row|<cell|12-345 Anytown, Poland>>|<row|<cell|+48
    123456789>>|<row|<cell|<hlink|<with|font-family|ss|jankowalski@gmail.com>|mailto:lukstafi@gmail.com>>>|<row|<cell|<hlink|<verbatim|http://uni.anytown.pl/~jankow>|http://ii.uni.wroc.pl/~lukstafi>>>>>>>
  </with>

  <tabular|<tformat|<cwith|10|10|1|1|cell-halign|r>|<cwith|12|12|3|3|cell-col-span|2>|<cwith|14|14|3|3|cell-col-span|2>|<cwith|15|15|3|3|cell-col-span|2>|<cwith|20|20|1|1|cell-halign|r>|<cwith|21|21|3|3|cell-col-span|2>|<cwith|1|1|3|3|cell-width|98mm>|<cwith|1|1|3|3|cell-hmode|exact>|<cwith|1|1|3|3|cell-hyphen|t>|<cwith|1|1|1|1|cell-hyphen|t>|<cwith|1|1|1|1|cell-width|12mm>|<cwith|1|1|1|1|cell-hmode|exact>|<cwith|6|6|4|4|cell-halign|r>|<cwith|7|7|3|3|cell-col-span|2>|<cwith|4|4|3|3|cell-col-span|2>|<cwith|3|3|3|3|cell-col-span|1>|<table|<row|<\cell>
    <strong|RESEARCH INTERESTS>
  </cell>|<cell|>|<\cell>
    <with|color|darker grey|Lorem ipsum dolor sit amet, consectetur
    adipiscing elit. Curabitur dapibus neque eget bibendum tempus. Praesent
    pellentesque venenatis laoreet. Duis at sem eget ipsum luctus vehicula.
    Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
    inceptos himenaeos. Sed euismod felis nisl, at facilisis diam ultricies
    sed. Nullam nunc orci, sagittis a ornare in, aliquam non erat. Integer
    venenatis, ligula nec sodales dignissim, augue turpis iaculis mauris,
    eget suscipit est nisi sit amet tellus. Fusce consectetur malesuada
    lacus, vitae fringilla leo aliquet lobortis. Curabitur mauris nibh,
    aliquet a lacus vel, varius iaculis felis. Sed vestibulum scelerisque
    sapien, ut consequat elit bibendum quis.>
  </cell>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<strong|EDUCATION>>|<cell|>|<cell|<strong|PhD
  in Computer Science> -- University of Anytown<htab|5mm>>|<cell|<em|2005-2010>>>|<row|<cell|>|<cell|>|<cell|<small|Thesis:
  <with|font-family|ss|Curabitur rhoncus magna at ante consequat
  auctor>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|>|<cell|<strong|M.Sc.
  Mathematics> -- Anytown University of Technology>|<cell|<em|1999-2005>>>|<row|<cell|>|<cell|>|<cell|<small|Broad
  mathematical curriculum. Thesis: <with|font-family|ss|Aliquam erat
  volutpat>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<strong|EXPERIENCE>>|<cell|>|<cell|>|<cell|>>|<row|<cell|<strong|Publications>>|<cell|>|<cell|<small|.
  Kaiser and . Stafiniak. <with|font-family|ss|Playing Structure Rewriting
  Games>.>>|<cell|>>|<row|<cell|>|<cell|>|<cell|<small|<em|In Proceedings of
  AGI '10. Atlantis Press, 2010.>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|<small|.
  Kaiser and . Stafiniak. <with|font-family|ss|First-Order Logic with
  Counting for General Game Playing>.>>|<cell|>>|<row|<cell|>|<cell|>|<cell|<small|<em|In
  Proceedings of the 25th AAAI Conference,
  2011.>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|<small|. Kaiser and .
  Stafiniak. <with|font-family|ss|Translating the Game Description Language
  to Toss>.>>|<cell|>>|<row|<cell|>|<cell|>|<cell|<small|<em|In Proceedings
  of the 2nd International General Game Playing
  Workshop,>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|<small|<em|GIGA'11,
  2011.>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|<small|. Stafiniak.
  <with|font-family|ss|Joint Constraint Abduction
  Problems>.>>|<cell|>>|<row|<cell|>|<cell|>|<cell|<small|<em|In UNIF 2011 --
  The International Workshop on Unification.>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<strong|Teaching>>|<cell|>|<cell|Lecturer:>|<cell|>>|<row|<cell|>|<cell|>|<cell|
  <tabular|<tformat|<table|<row|<cell|<with|font-family|ss|Functional
  Programming> -- <small|<hlink|<verbatim|http://www.ii.uni.wroc.pl/~lukstafi/FPcourse>|http://www.ii.uni.wroc.pl/~lukstafi/FPcourse>>>>|<row|<cell|<with|font-family|ss|Programming
  in Pascal>>>|<row|<cell|<with|font-family|ss|Programming in
  Java>>>>>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|Teaching
  assistant:>|<cell|>>|<row|<cell|>|<cell|>|<cell|
  <tabular|<tformat|<table|<row|<cell|<with|font-family|ss|Artificial
  Intelligence>>>|<row|<cell|<with|font-family|ss|Algorithmic Game
  Theory>>>|<row|<cell|<with|font-family|ss|Working in
  Linux>>>|<row|<cell|<with|font-family|ss|Data Stores and Data
  Mining>>>|<row|<cell|<with|font-family|ss|Multi-Agent
  Systems>>>|<row|<cell|<with|font-family|ss|Evolutionary
  Algorithms>>>|<row|<cell|<with|font-family|ss|Natural Language
  Processing>>>|<row|<cell|<with|font-family|ss|Neural
  Networks>>>>>>>|<cell|>>>>>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>