### Angular使用Promise

<div>前端</div>

<time>2018/2/6</time>

同步请求数据期间浏览器会“不响应”直至数据到达，所以新版本angular2中的Http实现则是强制异步。异步也是必要的，尤其是在客户端（javascript是单线程），想象一下用户请求一份远程数据，过了2秒才到，如果在同步的情况下，浏览器无法响应持续2秒钟，然后才进行下一步的渲染工作，破坏用户体验。

Angular强制其Http库异步处理，虽然可以防止开发者误用同步来获取远程数据或者做耗时操作，但是对于刚接触Angular的就不那么友好了。如果不是很了解Http的强制异步处理模式，很容易写出以下代码
```javascript
class DataServie {
    result = 'default';
    constructor(private http: Http){}
    getResult(){
        this.http.get('http://google.com.hk')
            .map(response => response.text())
            .subscribe(data => {
                this.result = data;
                console.log('inside of callback（后执行)');
            });
        console.log('outside of callback (先执行)');
    }
    
    doSomething() {
        console.log("do something to data: "+ this.result);
    }

    doThis(){
        getResult();  // 异步
        doSomething();  //读取 this.result 
                        // 发现依然是旧值。。。what？
    }
}
```
上述代码表面上是同步的，即一行代码执行完所有操作再执行下一行，然而Http的```get```函数却是异步的。
代码的输出结果违背直觉，在使用this.result时也会出现无法获取新值的窘境。这个坑，作为JS新手的我，反正踩了。

那么本文提供了：
 - 什么是异步
 - 什么是callback
 - 传统方法：用callback实现异步处理
 - Angular的推荐方法：用Promise实现异步处理

So，首先简单介绍一下JS
-----

JavaScript是这样介绍自己的：

> JavaScript is a single-threaded, non-blocking, asynchronous, concurrent language.

函数调用栈
--------

类似Java等通用语言的单线程代码。

```javascript
var makeCookies = function() { 
    // make them
}
var sellCookies = function() {
    makeCookies();
}
var myStoreOperation = function() {
    sellCookies();
}

```

调用栈
```javascript
myStroreOperation //先执行
sellCookies
makeCookies
```

这就是浏览器执行的单线程代码，如果中间任何一个步骤耗时太长，后续代码将被阻塞，导致页面卡顿。
像`console.log()`或者复杂度不高的数值计算加入到调用栈中不会对整体单线程执行速度造成影响。但是如果将不可预期的远程（http等）数据请求等耗时操作加入到单线程调用栈里，页面卡住了会严重破坏用户体验。

Callbacks(同步版本)
------

函数f作为参数传入另外一个函数g，函数f就称为回调函数。

写一个函数返回原数的2倍。

```javascript
var takeActionToNumber = function(n, action) {
    return action(n);
}
var doubleIt = function(num){
    return num *2;
}

takeActionToNumber(3, doubleIt); // 结果为6. 
                                  //doubleIt作为一个回调函数
```

`takeActionToNumber`里不包含异步代码。

Callbacks(异步版本)
------------

而存在异步代码的`setTimeout`
```javascript
setTimeout(() => console.log('5秒钟过去了，我才执行:('),
            5000);
console.log('我先进入调用栈，我先执行');
```

发生了什么？

![](https://gitlab.com/mavoid/writenotes/raw/master/assets/image/js-callstack.png)

哪个函数代码先后执行取决于进入调用栈的顺序，你可以把调用栈想象成一个队列，代码排队等待执行。

以前我们使用回调函数
--------

回调函数因为JS的单线程编程而大量使用的模式。回到文章开头的```DataServie```，想要对获取到的数据进行操作，一种方法就是把```doSomething```写成一个回调函数，以参数的形式传给  ```getResult```,在远程获取到到数据之后再执行```doSomething```。

修改前（文章开头代码复制过来）
```javascript
class DataServie {
    result = 'default';
    constructor(private http: Http){}
    getResult(){
        this.http.get('http://google.com.hk')
            .map(response => response.text())
            .subscribe(data => {
                this.result = data;
                console.log('inside of callback（后执行)');
            });
        console.log('outside of callback (先执行)');
    }
    
    doSomething() {
       console.log("do something to data: "+ this.result);
    }

    doThis(){
        getResult();  // 异步
        doSomething();  //读取 this.result 
                        // 发现依然是旧值。。。what？
    }
}
```

修改后
```javascript
class DataServie {
    result = 'default';
    constructor(private http: Http){}
    getResult(callback){
        this.http.get('http://google.com.hk')
            .map(response => response.text())
            .subscribe(data => {
                // this.result = data;
                callback(data);
                console.log('inside of callback（后执行');
            });
        console.log('outside of callback (先执行)');
    }
    
    doSomething() {
        console.log("do something to data: "+ this.result);
    }

    doThis(){
        getResult();  // 异步
        doSomething();  //读取 this.result 
                        // 发现依然是旧值。。。what？
    }
}
```

这是单层嵌套，嵌套多了会出现"callback hell"。

上边的例子有一些背景条件，在```getResult()```之后```doSomething()```马上被调用，这种情况下```this.result```来不及更新。想要新的值也可以，必须要保证```doSomething```在足够的时间间隔之后被执行，即与同步代码一样符合直觉的方式。但在实际环境中数据流很复杂的情况下根本无法保证。

所以下边的代码看看就行，千万不要学。

```javascript
    doThis(){
        getResult();  // 异步
        setTimeout(() => doSomething(), 3000); 
        //等3秒钟，等待异步代码执行完毕
        //然后执行 doSomething
    }
```

3秒之后异步代码如果没执行完毕呢？你的逻辑就又乱了：）

主角，Promise
----------

想象你女儿要出嫁了，准女婿许诺@一辈子，而你选择了相信他，但是，

> 如果女婿信守承诺，对你女儿好，你狠高兴。
> 如果女婿对女儿不好，你花钱雇了凶，打断他的腿。

你实际上创建了一个Promise。

Promise的3个状态
-----------

### 1. Pending

刚刚被创建时的状态。

你女儿出嫁当天，女婿承诺，但是还没做到。

### 2. Resolved

许诺被成功执行。

女婿一直对女儿好。

### 3. Rejected

许诺没被执行。

。。。

创建Promise

```javascript
let promise = new Promise((resolve, reject) => {
    //异步操作放这里

    //如果异步操作成功
    resolve();

    //如果失败
    reject();
});
```






