Netlify免费搭建静态网站
=======

<div>网站建设</div>

<time>2018/2/11</time>

Netlify是少数的几个免费支持HTTPS的静态托管平台之一，同时支持dns解析，搭建过程十分傻瓜，而且全程免费，对于个人博客非常适合，本网站就是通过Netlify搭建。

对比 ```GitHub Pages```,

<pre>
<table>
<thead>
<tr>
<th align="center"></th>
<th align="center"><img src="https://cdn.netlify.com/013d35039214e7be3a0b208c54ff507af4c13faa/85586/img/comparisons/gh-pages/gh-pages-logo.svg" alt="GitHub Pages" class="cld-responsive"></th>
<th align="center"><img src="https://cdn.netlify.com/1e66438712c70d3dfdcfe11f31e467864f94b803/b2c28/img/press/logos/full-logo-light.svg" alt="Netlify" class="cld-responsive"></th>
</tr>
</thead>

<tbody>
<tr>
<td align="center">Price</td>
<td align="center">Free</td>
<td align="center">Free</td>
</tr>

<tr>
<td align="center">Build Limits</td>
<td align="center">10 Builds per hour</td>
<td align="center">3 Builds per minute</td>
</tr>

<tr>
<td align="center">Custom Domain</td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">SSL for Custom Domains</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">1-click Rollbacks</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">Asset Optimizations</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">Form Handling</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">Deploy Previews</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">Continuous Deployment</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">Custom Rewrites &amp; Redirects</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">Compatible w/All Static Site Generators</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>

<tr>
<td align="center">Split Testing</td>
<td align="center"><img src="https://cdn.netlify.com/e23268b125c9134c9dcb04c8857e935f419c83f5/34f7f/img/comparisons/delete.svg" alt="No" class="cld-responsive"></td>
<td align="center"><img src="https://cdn.netlify.com/36c14a5d5e1f70f8fa7330c47fb0010c94415562/a37fb/img/comparisons/check.svg" alt="Yes" class="cld-responsive"></td>
</tr>
</tbody>
</table>
</pre>

```Netlify``` 支持通过Git仓库直接部署，十分方便。本文展示通过```angular2```项目搭建此网站的过程。

###### 买域名

你可以通过这个网站挑选域名 [域名比价](https://www.domcomp.com/?refcode=5a6dc7be120000bf7011cde5)。

##### 注册 Netlify

##### 创建新网站

![](https://gitlab.com/mavoid/writenotes/raw/master/assets/image/netlify-newsite.png)

右上角点击从Git仓库创建新网站

![](https://gitlab.com/mavoid/writenotes/raw/master/assets/image/netlify-bit.png)

选择你要托管的项目（我用angular构建的项目）

填写部署参数
![](https://gitlab.com/mavoid/writenotes/raw/master/assets/image/netlify-deploy.png)

部署成功
![](https://gitlab.com/mavoid/writenotes/raw/master/assets/image/netlify-done.png)

##### 后续工作（可选）

不管是通过Netlify自带的DNS解析(免费)还是第三方的DNS解析如 [Dnspod](https://www.dnspod.cn/) , [Cloudflare](https://www.cloudflare.com),你可以绑定自己的域名到Netlify。

##### 访问本站

[https://determined-einstein-3631f8.netlify.com](https://determined-einstein-3631f8.netlify.com)

[https://vimoo.com](https://vimoo.com)








