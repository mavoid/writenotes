个人简历
======

<div>Resume</div>

<time>2018-02-27</time>

#### 联系方式

- 手机：17611541933
- Email：mhy@live.cn


#### 个人信息

 - 马洪跃/男/1991
 - 本科/青岛大学
 - 工作年限：1年
 - blog：[https://vimoo.win](https://vimoo.win)
 - Github：[https://github.com/bryce-ma](https://github.com/bryce-ma)

 - 期望职位：Java工程师
 - 期望城市：北京
 - 技能特点：数据结构，算法基础好，linux环境熟练，技术涉及广泛，喜欢学习和使用新技术

#### 课程证书

  - [The Hardware/Software Interface](https://gitlab.com/mavoid/writenotes/raw/master/resume/Coursera_Certificate_v1-9726463959647.pdf) 华盛顿大学
  - [Algorithms: Design and Analysis, Part 1](https://gitlab.com/mavoid/writenotes/raw/master/resume/Coursera_Certificate_v1-9708273959647.pdf) 斯坦福大学
  - [Algorithms: Design and Analysis, Part 2](https://gitlab.com/mavoid/writenotes/raw/master/resume/Coursera_Certificate_v1-9723343959647.pdf) 斯坦福大学
  - [Coding the Matrix: Linear Algebra through Computer Science Applications](https://gitlab.com/mavoid/writenotes/raw/master/resume/Coursera_Certificate_v1-9702603959647.pdf) 布朗大学


#### 技能清单

以下均为我熟练使用的技能

- 编程语言：Java/Python/Bash/C/C++/Javascript/Mathematica/Mathlab/Scala/Scheme
- 前端框架：Bootstrap/Angular2/HTML5/Thymeleaf
- 后端框架：SpringMVC/SpringBoot
- 数据库：MySQL/Redis
- 版本管理、部署工具：Git/Docker
- 云和开放平台：AWS(EC2,S3)/Digital Ocean(Droplet,Spaces)
            
#### 工作经验

  - 公司：北京法海网
  - 职位：后端开发工程师
  - 时间：2015-02至2015-09
  - 职责：负责后端API的书写，提供给iOS和Android客户端调用
  - 使用技术：环信/SpringMVC/MySQL/Git/Redis


    
    